<?php
Class Common_model extends CI_Model
{
	function login($unm,$password)
	{
	   $this -> db ->select('*');
	   $this -> db ->from('login');
	   $this -> db ->where('username',$unm);
	   $this -> db ->where('password',md5($password));	   
	   $this -> db ->where('status', '1'); 
	   $this -> db ->limit(1);
	 
	   $query = $this -> db -> get();
	 
	   if($query -> num_rows() == 1)
	   {
		 return $query->result(); 
	   }
	   else
	   {
		 return false; 
	   }
	}
	function app_login($unm,$password) 
	{
	   $this -> db ->select('*');
	   $this -> db ->from('login');
	   $this -> db ->where('username',$unm);
	   $this -> db ->where('password',md5($password));	  
	   $this -> db ->where('user_type','front');  
	   $this -> db ->where('status', '1'); 
	   $this -> db ->limit(1); 
	 
	   $query = $this -> db -> get();
	 
	   if($query-> num_rows() == 1)
	   {
		 return $query->result(); 
	   }
	   else
	   {
		 return 'no result'; 
	   }
	}
	function get_single_limit($table,$data,$where,$orderby,$val)
	{
       $this -> db ->select($data); 
	   $this -> db ->from($table);
	   $this -> db ->where($where);  
	   $this->db->order_by($orderby,$val);
	   $this -> db ->limit(1);
       $query = $this->db->get();
		return $query->row();
	}
	function get_single_unique($table,$data,$userid,$email)
    { 
		$this->db->select($data);
		$this->db->from($table);
		$this->db->where('id !=',$userid);
		$this->db->where('email',$email);
		$this->db->where('status',1);
		$query = $this->db->get();
		return $query->row();
	}
	
	function get_emp($table,$data,$userid,$empno)
    { 
		$this->db->select($data);
		$this->db->from($table);
		$this->db->where('id !=',$userid);
		$this->db->where('emp_no',$empno);
		$this->db->where('status',1);
		$query = $this->db->get();
		return $query->row();
	}
	function insert_data($table,$data)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}
	
	function get_data($table,$data)
	{
	   $this->db->select($data);
	   $this->db->from($table);
	   $query = $this->db->get();
	   return $query->result();
    }
    function get_single($table,$data,$where)
    { 
		$this->db->select($data);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->row();
	}
    function get_all_where($table,$data,$where)
    {
		$this->db->select($data);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	} 
	function get_all_where_order($table,$data,$where,$orderby,$val){
    
       $this->db->select($data);
	   $this->db->from($table);
	   $this->db->where($where);
	   $this->db->order_by($orderby,$val);
	   $query = $this->db->get();
	   return $query->result(); 
	}
	 function updates($table,$data,$where)

	{
		if($this->db->where($where)->update($table,$data))
		{
			return "updated";
		}
		else
		{
			return false;
		}
	}
     function get_recordByid($table,$data)
	 {          
        //Ordering Data
            //Executing Query
            $query = $this->db->get_where($table,$data);
    
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return array();
            }
        }

	function delete($table,$where)
	{
		$this->db->where($where);
        $this->db->delete($table);
	}
	
	function unique_check_group($table,$data,$uid,$uname){
	       
	    $this->db->select($data);
		$this->db->from($table);
        $this->db->where('user_id !=',$uid);
        $this->db->where('user_name',$uname);
        $this->db->where('is_delete','0'); 
        $query = $this->db->get();
		return $query->row(); 
	}    
   	function get_join_data($table1,$table2,$val1,$val2,$data,$where,$where_val,$where1,$whereone)
	{
		$this->db->select($data);
		$this->db->from($table1);
		$this->db->join($table2,"$table1.$val1 = $table2.$val2"); 
		$this->db->where($where,$where_val);
		$this->db->where($where1,$where_val);
		$query = $this->db->get(); 
		return $query->result();

	}
    
    function row_delete($table,$where)
	{
	   $this->db->where($where);
	   $this->db->delete($table); 
	}


}
?>
