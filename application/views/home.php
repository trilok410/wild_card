<!--Main container sec start-->
    <div class="main_container">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-4">
                	<div class="sidebar">
                    	<div class="list-group">
                            <div class="list-group-item">
                                <h4>Applications</h4>
                            </div>
                            <div class="list-group-item">
                                <ul class="nav nav-pills">
                                    <li><a href="<?php echo base_url('card'); ?>">Cards</a></li>
                                    <li><a href="#">Decks</a></li>
                                    <li><a href="#">Lobbies</a></li>
                                     
                                </ul>
                            </div>
                            <div class="list-group-item">
                                <h4>Promotions</h4>
                                <p><button class="btn btn-block btn-info xtrigger">New Promotion</button></p>
                            </div>
                            <div class="list-group-item">
                                <h4>dsfsgfg</h4>
                                <h6>sdfgsdfg  - Reward $ 4</h6>
                                <p><button  class="btn btn-danger btn-block xtrigger"><i class="fa fa-trash"></i> Delete</button></p>
                            </div>
						</div>
                    </div>
                </div>
                <div class="col-sm-8">
                	<div class="right_sidebar">
                      <h4>Wildcards</h4>
                      <a class="btn btn-warning btn-block" href="#">View Transactions</a>
                      <div class="form-group">
                      	<p class="text-muted">Push Notifications</p>
                      </div>
                      <div class="form-group">
                      	<textarea placeholder="Push text content" class="form-control not-box-text"></textarea>
                      </div>
                      <div class="form-group">
                      	<button class="btn btn-block btn-info not-box">Push</button>
                      </div>
 
					  <div class="list-group"> 
           				 <div class="list-group-item">
                                <h4>Lobby Categorization</h4>
                                <div class="form-group">
                                    <p class="text-muted">Use this button below to make new categories to classify lobbies by.</p>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-block btn-info xtrigger">New lobby group</button>
                                </div>
                     	</div>
				    	 <div class="list-group-item">
                                <h4>50/50 NFL</h4>
                                <div class="form-group"> 
                                    <p class="text-muted">Payout Map </p>
                                 </div>
                                <div class="form-group">
                                    <button  class="btn btn-danger btn-block xtrigger">
                                    <i class="fa fa-trash"></i> Delete</button>
                                </div>
                         </div>
						 <div class="list-group-item">
                            	<h4>Price Structure</h4>
                                <p class="text-muted">Payout Map</p>
                                <p>1st. $ 10.00</p>
                                <p>2nd. $ 5.00</p>
                                <p>3rd. $ 1.00</p>
                                <p><button class="btn btn-danger btn-block xtrigger"><i class="fa fa-trash"></i> Delete</button></p>
                        </div>
					  </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Main container sec end-->