    <!--Main container sec start--> 
    <div class="main_container">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-4">
                	<div class="sidebar">
                    	<div class="list-group">
                            <div class="list-group-item">
                                <h4>Cards</h4>
                            </div>
                            <div class="list-group-item">
                                <ul class="nav nav-pills"> 
                                    <li><a href="javascript:window.history.go(-1);"><span class="fa fa-arrow-left"></span> Back</a></li>

                                    <li><a href="<?php echo base_url('card/new_card'); ?>">New Card</a></li>
                                </ul>
                            </div>
						</div>
                        <div class="card_list" id="jstree_card">
                        	
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                	<div class="right_sidebar">
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Main container sec end-->
  </main>
 <!-- modal for create folder -->
 <div class="modal fade" id="createFolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
        <h4 id="myModalLabel" class="modal-title"><i class="fa fa-globe fa-spin"></i></h4>
      </div>
      <div class="modal-body">
      <div class="marker-stem">
			<div class="marker">
			<h3>New Root folder</h3>
			<div class="form-group">
            	<input placeholder="Folder name" name="rootfoldername" class="form-control xsub required">
            </div>
            <div class="form-group">
				<p class="loader-item"><button class="btn btn-block btn-primary dosub">Create</button></p>
			</div>
		</div>
		</div>
		
		</div>
      <div class="modal-footer">
      
        <button data-dismiss="modal" class="btn btn-default " type="button">Close</button>
       
      </div>
    </div>
  </div>
 </div>
  