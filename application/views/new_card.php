    <!--Main container sec start-->
    <div class="main_container"> 
    	<div class="container">
        	<div class="row">

            	<div class="col-sm-4">
                	<div class="sidebar">
                    	<div class="list-group">
                            <div class="list-group-item">
                                <h4>Cards</h4>
                            </div>
                            <div class="list-group-item">
                                <ul class="nav nav-pills">
                                    <li><a href="javascript:history.back()"><span class="fa fa-arrow-left"></span> Back</a></li>
                                    <li><a href="<?php echo base_url('card/new_card'); ?>">New Card</a></li>
                                </ul>
                            </div>
						</div>
                        
                        <div class="card_list" id="jstree_card">
                        	<!--<ul>
                            <li>Root node 1 
                            	<ul>
                                <li>Child node 1</li>
                                <li><a href="#">Child node 2</a></li>
                              </ul>
                            </li>
                            <li>Root node 2</li>
                          </ul>-->
                        </div>
                    </div>
                </div>
                <form role="form" enctype="multipart/form-data" method="post">
                <div class="col-sm-8">
                	<div class="right_sidebar">
                      	<div class="newcard_form">
                        	
                            	<div class="form-group">
                                	<input type="text" class="form-control" name="name_football_player" placeholder="New Card">
                                </div>
                                <div class="form-group">
                                	<label>Explicit card type</label>
                                    <select class="form-control" name="type_card">
                                        <option value="wildcard">Wildcard</option>
                                        <option value="off">Offensive</option>
                                        <option value="def">Defensive</option>
                                        <option value="team">Team</option>
                                        <option value="evt">Event</option>
                                    </select> 
                                </div> 
                                <div class="form-group">
                                	<label>If this card is not linked to a specific player but rather a team, please input the team's abbreviation below.</label>
                                   <input type="text" class="form-control" placeholder="Team Name" name="name_of_team">
                                </div>
                                <div class="form-group">
                                	<label><span class="fa fa-cube"></span> Card points</label>
                                    <input type="number" class="form-control" placeholder="Card Points" name="card_points">
                                </div>
                                <div class="form-group">
                                	<nav class="newcard_btn">
                                    	<ul>
                                        	<li><span class="btn btn-primary"> Add Image <input name="playerpic" type="file"></span></li>
                                            <li><a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#addpoint_condition">add point condition</a></li>
                                            <li><a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#link_card"><span class="fa fa-street-view"></span>Link card</a></li>
                                        </ul>
                                    </nav>
                                </div>
                                <input type="hidden" id="form_team_name" name="form_team_name">
                                <input type="hidden" id="form_point_reward" name="form_point_reward">   
                                <input type="hidden" id="form_point_cat" name="form_point_cat">
                                <input type="hidden" id="form_team_name_abb" name="form_team_name_abb">
                                <input type="hidden" id="form_y_value" name="form_y_value">

                                <p class="loader-item">
                                    <button class="btn btn-block btn-success dosub" name="submit" type="submit">Update</button>
                                </p> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Main container sec end-->
  </main>
 
    <!-- modal for add point condition -->
     <div class="modal fade" id="link_card" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="myModalLabel" class="modal-title"><i class="fa fa-globe fa-spin"></i></h4>
          </div>
          <div class="modal-body">
            <div class="addpointcon_form">
                    <div class="form-group">
                        To start please enter the desired player's team name abbreviation
                    </div>
                    <div class="form-group">
                        <input name="team_name" id="team_name" type="text" class="form-control" plcaholder="Team name abbreviation">
                    </div>
                    <div class="form-group">
                        <input type="button" id="link_card_id" class="btn btn-block btn-primary" value="Add">
                    </div> 
            </div>
            </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default">Close</button>
          </div>
        </div> 
      </div>
 </div>
 <!-- modal for add point condition -->
 <div class="modal fade" id="addpoint_condition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
        <h4 id="myModalLabel" class="modal-title"><i class="fa fa-globe fa-spin"></i></h4>
      </div>
      <div class="modal-body">
      	<div class="addpointcon_form">
        	
            	<div class="form-group">
                	<label>Point reward for each event occurance</label>
                    <input type="number" id="point_reward" name="point_reward" class="form-control">
                </div>
                <div class="form-group">
                <select name="point_cat" id="point_cat" class="form-control">
                	<option name="per_reception" value="receiving/rec[@Per Reception">Per Reception</option>
                    <option name="per_completion" value="passing/cmp[@Per completion">Per completion</option>
                 </select>
                </div> 
                <div class="form-group">
                	<input type="text" id="team_name_abb" name="team_name_abb" class="form-control" placeholder="Team name abbreviation.">
                </div>
                <div class="form-group">
                	*if applicable please enter a Y value. Player X refers to the player or team in comparison. Please use the player finder below to Pick a player. 
                </div>
                <div class="form-group">
                	<input name="y_value" id="y_value" type="number" class="form-control" placeholder="Y Value">
                </div>
                <div class="form-group">
                	<input type="button" id="point_re_id" class="btn btn-block btn-primary" value="Add">
                </div>
            
        </div>
		
		</div>
      <div class="modal-footer">
      
        <button data-dismiss="modal" class="btn btn-default " >Close</button>
       
      </div>

      </form>

    </div>
  </div>
 </div>