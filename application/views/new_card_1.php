
    <!--Main container sec start-->
    <div class="main_container">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-4">
                	<div class="sidebar">
                    	<div class="list-group">
                            <div class="list-group-item">
                                <h4>Cards</h4>
                            </div>
                            <div class="list-group-item">
                                <ul class="nav nav-pills">
                                    <li><a href="javascript:history.back()"><span class="fa fa-arrow-left"></span> Back</a></li>
                                    <!--<li><a href="javascript:void(0)" data-toggle="modal" data-target="#createFolder">New root folder</a></li>-->
                                    <li><a href="<?php echo base_url('new_card'); ?>">New Card</a></li>
                                </ul>
                            </div>
						</div>
                        
                        <div class="card_list" id="jstree_card">
                        	<!--<ul>
                            <li>Root node 1
                            	<ul>
                                <li>Child node 1</li>
                                <li><a href="#">Child node 2</a></li>
                              </ul>
                            </li>
                            <li>Root node 2</li>
                          </ul>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                	<div class="right_sidebar">
                      	<div class="newcard_form">
                        	<form role="form">
                            	<div class="form-group">
                                	<input type="text" class="form-control" placeholder="New Card">
                                </div>
                                <div class="form-group">
                                	<label>Explicit card type.</label>
                                    <select class="form-control">
                                        <option value="wildcard">Wildcard</option>
                                        <option value="off">Offensive</option>
                                        <option value="def">Defensive</option>
                                        <option value="team">Team</option>
                                        <option value="evt">Event</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                	<label>If this card is not linked to a specific player but rather a team, please input the team's abbreviation below.</label>
                                   <input type="text" class="form-control" placeholder="Team Name">
                                </div>
                                <div class="form-group">
                                	<label><span class="fa fa-cube"></span> Card points</label>
                                    <input type="number" class="form-control" placeholder="Card Points">
                                </div>
                                <div class="form-group">
                                	<nav class="newcard_btn">
                                    	<ul>
                                        	<li><span class="btn btn-primary"> Add Image <input type="file"></span></li>
                                            <li><a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#addpoint_condition">add point condition</a></li>
                                            <li><a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#link_card"><span class="fa fa-street-view"></span>Link card</a></li>
                                        </ul>
                                    </nav>
                                </div>
                                <p class="loader-item">
                                    <button class="btn btn-block btn-success dosub">Update</button>
                                </p> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Main container sec end-->
  </main>
  
 <!-- modal for create folder -->
 <div class="modal fade" id="createFolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
        <h4 id="myModalLabel" class="modal-title"><i class="fa fa-globe fa-spin"></i></h4>
      </div>
      <div class="modal-body">
      <div class="marker-stem">
			<div class="marker">
			<h3>New Root folder</h3>
			<div class="form-group">
            	<input placeholder="Folder name" name="rootfoldername" class="form-control xsub required">
            </div>
            <div class="form-group">
				<p class="loader-item"><button class="btn btn-block btn-primary dosub">Create</button></p>
			</div>
		</div>
		</div>
		
		</div>
      <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-default " type="button">Close</button>
      </div>
    </div>
  </div>
 </div>
 
 
 <!-- modal for add point condition -->
 <div class="modal fade" id="addpoint_condition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
        <h4 id="myModalLabel" class="modal-title"><i class="fa fa-globe fa-spin"></i></h4>
      </div>
      <div class="modal-body">
      	<div class="addpointcon_form">
        	<form role="form">
            	<div class="form-group">
                	<label>Point reward for each event occurance</label>
                    <input type="number" class="form-control">
                </div>
                <div class="form-group">
                <select class="form-control">
                	<option value="receiving/rec[@Per Reception">Per Reception</option>
                    <option value="passing/cmp[@Per completion">Per completion</option>
                    <option value="receiving|passing|rushing/td|td|td[@Per Rushing/Passing/Receiving TD">Per Rushing/Passing/Receiving TD</option>
                    <option value="defense/int[@Per interception">Per interception</option>
                    <option value="passing/yds]@For every Y yards passing">For every Y yards passing</option>
                    <option value="rushing/yds]@For every Y yards rushing">For every Y yards rushing</option>
                    <option value="receiving/yds]@For every Y yards receiving">For every Y yards receiving</option>
                    <option value="rushing/yds_++_plus]@For every rush of Y+ yards">For every rush of Y+ yards</option>
                    <option value="receiving|passing|rushing/fd|fd|fd[@For every 1st down conversion">For every 1st down conversion</option>
                    <option value="receiving/yac]@For every Y Yac">For every Y Yac</option>
                    <option value="defense|defense/comb|sp_comb[@Per tackle">Per tackle</option>
                    <option value="defense/sack[@Per sack">Per sack</option>
                    <option value="defense/tloss[@Per tackle for loss">Per tackle for loss</option>
                    <option value="defense|defense/fum_rec|int[@Per turnover forced">Per turnover forced</option>
                    <option value="passing/yds+@Will have Y or more yards passing">Will have Y or more yards passing</option>
                    <option value="passing/yds-@Will have Y or fewer yards passing">Will have Y or fewer yards passing</option>
                    <option value="receiving/yds+@will have Y or more yards receiving">will have Y or more yards receiving</option>
                    <option value="receiving/yds-@Will have Y or fewer yards receiving">Will have Y or fewer yards receiving</option>
                    <option value="rushing/yds+@Will have Y or more yards rushing">Will have Y or more yards rushing</option>
                    <option value="rushing/yds-@Will have Y or fewer yards rushing">Will have Y or fewer yards rushing</option>
                    <option value="rushing|receiving/yds|yds+@Will have Y or more yards rushing + receiving">Will have Y or more yards rushing + receiving</option>
                    <option value="rushing|receiving/yds|yds-@Will have Y or fewer yards rushing + receiving">Will have Y or fewer yards rushing + receiving</option>
                    <option value="receiving|passing|rushing/td|td|td>@Will record a TD">Will record a TD</option>
                    <option value="receiving|passing|rushing/td|td|td=@Will have Y tds">Will have Y tds</option>
                    <option value="defense/int=@Will have Y INT">Will have Y INT</option>
                    <option value="defense|defense/comb|sp_comb+@Will have Y or more tackles">Will have Y or more tackles</option>
                    <option value="defense/int>@Will have an interception">Will have an interception</option>
                    <option value="defense|defense/fum_rec|int+@Will force Y or more turnovers">Will force Y or more turnovers</option>
                    <option value="passing/cmp+@Will have Y or more completions">Will have Y or more completions</option>
                    <option value="defense/sack+@Will have Y or more sacks">Will have Y or more sacks</option>
                    <option value="rushing/yds*@will have more yards rushing than (Player X)">will have more yards rushing than (Player X)</option>
                    <option value="receiving/yds*@Will have more yards receiving than (Player X)">Will have more yards receiving than (Player X)</option>
                    <option value="passing/yds*@Will have more yards passing than (Player X)">Will have more yards passing than (Player X)</option>
                    <option value="rushing|receiving/yds|yds*@Will have more yards rushing and receiving than (Player X)">Will have more yards rushing and receiving than (Player X)</option>
                    <option value="defense/comb*@Will have more tackles than (Player X)">Will have more tackles than (Player X)</option>
                    <option value="receiving|passing|rushing/td|td|td*@Will have more tds than (Player X)">Will have more tds than (Player X)</option>
                    <option value="defense/qh[@Per QB Hit">Per QB Hit</option>
                    <option value="receiving/rec+@Will have Y or more receptions">Will have Y or more receptions</option>
                    <option value="receiving/rec*@Will have more receptions than (Player X)">Will have more receptions than (Player X)</option>
                    <option value="receiving|rushing/yds|yds]@For every Y yards rushing + receiving">For every Y yards rushing + receiving</option>
                    <option value="defense/fum_rec*@Will have more turnovers than (Player X)">Will have more turnovers than (Player X)</option>
				</select>
                </div> 
                <div class="form-group">
                	<input type="text" name="team_name" class="form-control" placeholder="Team name abbreviation.">
                </div>
                <div class="form-group">
                	*if applicable please enter a Y value. Player X refers to the player or team in comparison. Please use the player finder below to Pick a player. 
                </div>
                <div class="form-group">
                	<input name="number" type="number" class="form-control" placeholder="Y Value">
                </div>
                <div class="form-group">
                	<input type="submit" class="btn btn-block btn-primary" value="Add">
                </div>
            </form>
            	
        </div>
		
		</div>
      <div class="modal-footer">
      
        <button data-dismiss="modal" class="btn btn-default " type="button">Close</button>
       
      </div>
    </div>
  </div>
 </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugin.js"></script>
    <script src="js/custom.js"></script>
    
    <script>
		$(document).ready(function(e) {
            $(function () { $('#jstree_card').jstree(); });
        });
	</script>
    
  </body>
</html>