<main class="animsition">
		<div class="login_wrapper">
        	<div class="login_wrapper_inner">
            	<div class="login_blcok">
                	<div class="login_top">
                	<h3>Login</h3>
                    </div>
                    <div class="login_body">
                    <?php if($error !='') { ?>
                        <div class="alert alert-danger">
                           <strong>Warning! </strong><?php echo $error; ?>
                        </div>   
                    <?php } ?>
                	<form action="<?php echo base_url('/'); ?>" role="form" method="post">
                	<div class="list-group">
                    	<div class="list-group-item">
                        	<div class="form-group">
                            	<div class="input-group input-group-lg">
                                	<span class="input-group-addon">Username</span>
                                    <input type="text" name="unm" class="form-control" placeholder="Username">
                                </div>
                            </div>
                        </div>
                        <div class="list-group-item">
                        	<div class="form-group">
                            	<div class="input-group input-group-lg">
                                	<span class="input-group-addon">Password</span>
                                    <input type="password" name="psw" class="form-control" placeholder="Password">
                                </div>
                            </div>
                        </div>
                        <div class="list-group-item">
                        	<div class="form-group">
                                <input type="submit" class="btn btn-block btn-danger btn-lg" value="login">
                            </div>
                        </div>
                    </div>
                    </form>
                    </div>
                    <div class="login_bot">	
                    	<p>Wildcards Management portal login</p>
                    </div>
                </div>
            </div>
        </div>
  </main>