<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?php echo $title; ?></title>
    
    <!-- Bootstrap -->
    <link type="text/css" href="<?php echo site_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link type="text/css" href="<?php echo site_url('assets/css/custom.css') ?>" rel="stylesheet">
    <link type="text/css" href="<?php echo site_url('assets/css/responsive.css') ?>" rel="stylesheet">
    <link type="text/css" href="<?php echo site_url('assets/css/animsition.min.css') ?>" rel="stylesheet">
    <link type="text/css" href="<?php echo site_url('assets/css/plugin.css') ?>" rel="stylesheet">
    <link type="text/css" href="<?php echo site_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link type="text/css" href="<?php echo site_url('assets/css/stylesheet.less') ?>" rel="stylesheet">
    

    <!------------ Designer Plugin -->
    <script src="<?php echo site_url('assets/js/less.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/js/modernizr-custom.js') ?>"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <main class="animsition">
  <?php if($title!='Login') { ?>
  <!--Header sec start-->
    <header class="header_sec" id="header">
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                  <div class="header_inner">
                      <button  class="btn btn-danger btn-lg xtrigger" data-toggle="modal" data-target="#createUserModal">ACCOUNT</button>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--Header sec end--> 
    <?php } ?> 