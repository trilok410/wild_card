 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
    $config['title'] = 'Login'; 
    $config['error'] = '';

    $this->form_validation->set_rules('unm','Email','trim|required|xss_clean');
    $this->form_validation->set_rules('psw','Password','trim|required|xss_clean');
    
    $unm = $this->input->post('unm');
    $password = $this->input->post('psw');  

    if($this->form_validation->run() == TRUE)
    { 
        $check_login = $this->common_model->login($unm,$password);
         
        if(!empty($check_login))
        { 
                    $newdata = array(
                      'username'=> $check_login[0]->username, 
                      'logged_in' => TRUE 
                    );
                    $this->session->set_userdata('login',$newdata);   
                    $user_role = $this->session->userdata('login');  
          			redirect('home');   
        }else{
             $config['error'] =  'Wrong Login Credentials';  
        }     
    } 
        $this->load->view('common/header',$config);
		$this->load->view('login',$config);
		$this->load->view('common/footer',$config);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */