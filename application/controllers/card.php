<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Card extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$config['title'] = 'Card';  
        $config['error'] = '';
 
        $this->load->view('common/header',$config);
        $this->load->view('card',$config);
        $this->load->view('common/footer',$config);
	} 
  /* public function new_card()
	{
		$config['title'] = 'New Card';  
        $config['error'] = '';  
	if(isset($_POST['submit'])){
		$insertArray=array();
		$insertArray['name_football_player']=$_POST['name_football_player'];
		$insertArray['name_of_team']=$_POST['name_of_team'];
		$insertArray['type_card']=$_POST['type_card'];
		$insertArray['point_condition_id']=$_POST['point_condition_id'];
		if(isset($_POST['card_links_to'])){
		$insertArray['card_links_to']=$_POST['card_links_to'];
		}
		if(isset($_POST['prize'])){
		$insertArray['prize']=$_POST['prize'];
		}
		if(isset($_POST['cost_card'])){
		$insertArray['cost_card']=$_POST['cost_card'];
	    }
	    if(isset($_POST['point_reward'])){
		$insertArray['point_reward']=$_POST['point_reward'];
		}
		if(isset($_POST['point_categories'])){
		$insertArray['point_categories']=$_POST['point_categories'];
		}
		if(isset($_POST['team_name'])){
		$insertArray['team_name']=$_POST['team_name'];
		}
		if(isset($_POST['y_value'])){
		$insertArray['y_value']=$_POST['y_value'];
		}
		if(isset($_POST['link_card_team_name'])){
		$insertArray['link_card_team_name']=$_POST['link_card_team_name'];
		}
		$this->common_model->insert_data('card',$insertArray);

	}
		$this->load->view('common/header',$config);
        $this->load->view('new_card',$config);
        $this->load->view('common/footer',$config);
	} 
*/
   public function new_card()
	{
		$config['title'] = 'New Card';  
        $config['error'] = '';  
	    
	    if(!empty($_POST))
        {
            print($_POST); 

        }
        
		$this->load->view('common/header',$config);
        $this->load->view('new_card',$config);
        $this->load->view('common/footer',$config);
	} 
   

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
