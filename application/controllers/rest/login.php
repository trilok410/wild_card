<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Login extends REST_Controller{
    //your method name is different from the name of controller class
    
    public function log_get() { 
      
      $this->load->library('form_validation');

      $this->form_validation->set_rules('username', 'Username', 'required');
	  $this->form_validation->set_rules('password', 'Password', 'required');

	  $username =  $this->input->get('username');
	  $password = $this->input->get('password');

     if(($username!='') || ($password!=''))
     {		
     	$result = $this->common_model->app_login($username,$password); 

		if($result!='no result') 
		   {
		    $sess_array = array();
			foreach($result as $row)  
			{				 			   
				//redirect according to the user type
					   $sess_array = array(
						 'user_id'  => $row->id,
						 'user_name' => $row->username,
						 'status'=>1
					   );
					  $this->session->set_userdata('logged_in', $sess_array);  
                       echo $this->response($sess_array);   
			   }		 
		} 
		else
		{ 
		      // echo 'wrong username and password';
		      echo $this->response(array('message'=> 'Invalid username and Password','status'=>0), 200);
		} 
      }else{
      	     echo $this->response(array('username'=> 'username is required','password'=>'password is required','status'=>0), 200);
      } 
 
    }
}
?>
