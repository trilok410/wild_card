<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

class Register extends REST_Controller{
    //your method name is different from the name of controller class
    
    public function reg_get() { 
     
    $this->form_validation->set_rules('username', 'Username', 'required');
	  $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');

	  $username =  $this->input->get('username');
	  $password = $this->input->get('password');
	  $email = $this->input->get('email');

     if(($username!='') || ($password!='') || ($email!=''))
     {		
        $data = array(
        'username'=>$username,
        'password'=>md5($password),
        'Email'=>$email,
        'status'=>1
        ); 
     	$insert_id = $this->common_model->insert_data('user',$data);
       
        $d = array(
        'user_id'=>$insert_id,
        'username'=>$username,
        'password'=>md5($password),
        'user_type'=>'front',
        'status'=>1
        ); 
        $this->common_model->insert_data('login',$d);

        echo $this->response(array('user_id'=>$insert_id,'username'=>$username,'useremail'=>$email,'message'=>'success','status'=>1), 200);
      }else{
      	echo $this->response(array('username'=> 'username is required','password'=>'password is required','email'=>'email is required','status'=>0), 200);
      } 
 
    }
}
?>
